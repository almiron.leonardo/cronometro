let temporizador = document.getElementById("temporizador");
let iniciar = document.getElementById("startStop");
let reset = document.getElementById("reset");


let tiempo = 0, intervalo = 0;
let verificador = false;

init();

function init(){
    iniciar.addEventListener("click", iniciarContador);
    reset.addEventListener("click", resetContador);
}

function iniciarContador(){
    if(verificador == false){
        intervalo = setInterval(function(){
            tiempo += 0.01;
            temporizador.innerHTML = tiempo.toFixed(2);
        }, 10);
        verificador = true; 
    } else {
        verificador = false;
        clearInterval(intervalo);
    }
}

function resetContador(){
    verificador = false;
    tiempo = 0;
    temporizador.innerHTML = tiempo + ".00";
    clearInterval(intervalo);
}






